// Use the let keyword in an example function

function exampleFunctionUsingLet() {
  let number = 2;
  console.log(number);
}
exampleFunctionUsingLet();

// Use the const keyword in an example function

function exampleFunctionUsingConst() {
  const number = 2;
  console.log(number);
}
exampleFunctionUsingConst();

// Create an arrow function that finds the square of a number

const squareNumber = (number) => {
  return number * number;
};

console.log(squareNumber(6));

// Create an arrow function that adds two numbers

const addingTwoNumber = (number1, number2) => {
  return number1 + number2;
};

console.log(addingTwoNumber(5, 7));

// Create a multi-line string and then split the string into the corresponding lines and print the lines.
let stringData = `Lorem ipsum dolor sit amet, consectetur adipiscing elit. 
Aliquam quis elit nisl. 
Aliquam congue ut orci quis gravida. `;

const arrayOfStrings = stringData.split("\n");

for (let string of arrayOfStrings) {
  console.log(string);
}

// Create a function that calculates the area of a circle. If the radius of the circle is not provided assume that the default radius is 5. Use the JavaScript default parameter feature to implement the function

function calculateCircleArea(radius = 5) {
  return Math.PI * radius * radius;
}

console.log(calculateCircleArea(8));

// let person = {
//     name: 'Harry Potter',
//     location: 'London'
// }
// Create a string that prints the name and location of the person in below format:
// "Harry Potter is located in London."

let person = {
  name: "Harry Potter",
  location: "London",
};

let personInfo = `${person.name} is located in ${person.location}.`;

// Show an example where an array is destructured using destructuring assignment.

const numbers = [1, 2, 3];
const [value1, value2, value3] = numbers;
console.log(value1, value2, value3);

// Show an example where an object is destructured using destructuring assignment in the function body

function showingPersonDetails(personData) {
  const { name, location } = personData;
  const personInfo = `${name} is located in ${location}.`;
  console.log(personInfo);
}

let personData = {
  name: "Harry Potter",
  location: "London",
};

showingPersonDetails(personData);

// Show an example where a function argument which is an object is destructured inside the parantheses of the function

function displayPersonDetails({ name, location }) {
  const personInfo = `${name} is located in ${location}.`;
  console.log(personInfo);
}

let personInformation = {
  name: "Harry Potter",
  location: "London",
};

displayPersonDetails(personInformation);

// Show an example where enhanced object literals is used.

let infoOfPerson = {
  name: "Harry Potter",
  location: "London",
  displayPersonDetail: function () {
    return `${this.name} is located in ${this.location}.`;
  },
};
console.log(infoOfPerson.displayPersonDetail());

// Create a function sum that takes any number of numbers as arguments and calculates the sum of the input numbers using the rest parameter syntax
function sumOfNumbers(...numbers) {
  return numbers.reduce((acc, number) => {
    return acc + number;
  }, 0);
}

console.log(sumOfNumbers(1, 2, 3, 4));

// Use the spread syntax to expand an array of numbers and pass the elements of the array as arguments to the sum function created in the previous example

function sumOfNumbers(...numbers) {
  return numbers.reduce((acc, number) => {
    return acc + number;
  }, 0);
}
let data = [1, 2, 3, 4];
console.log(sumOfNumbers(...data));

// Use the for..of loop to iterate through all values in an array

const values = [1, 2, 3, 4];
for (let value of values) {
  console.log(value);
}

// Iterate through all keys of an object using Object.keys
const personObjectInfo = {
  name: "Harry Potter",
  location: "London",
};

for (let key of Object.keys(personObjectInfo)) {
  console.log(key);
}

// Iterate through all values of an object using Object.values
const personObjectData = {
  name: "Harry Potter",
  location: "London",
};

for (let value of Object.values(personObjectData)) {
  console.log(value);
}

// Iterate through all the key / value pairs of an object using Object.entries
const personObjectInformation = {
  name: "Harry Potter",
  location: "London",
};

for (let [key, value] of Object.entries(personObjectInformation)) {
  console.log(`${key}:${value}`);
}
