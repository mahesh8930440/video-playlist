const matchesWonPerTeamPerYear=require("../src/server/2-matches-won-per-team-per-year");

test("matches-won-per-team-per-year",()=>{
  const testSampleData=[{
    id:"1",
    season:"2008",
    winner:"CSK"
  },
  {
    id:"2",
    season:"2009",
    winner:"CSK"
  },
  {
    id:"3",
    season:"2008",
    winner:"MI"
  },
  {
    id:"4",
    season:"2015",
    winner:"CSK"
  },
  {
    id:"5",
    season:"2008",
    winner:"MI"
  }
];
const resultData = { 2008:{CSK: 1,MI:2}, 2009:{CSK: 1}, 2015: {CSK:1}};
expect(matchesWonPerTeamPerYear(testSampleData)).toEqual(resultData);
});
