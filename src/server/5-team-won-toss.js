const {allDeliveries,allMatches}= require("../data/allDeliveriesAndMatchesData");

function teamWinTossAndWinMatch(allMatches){
    const TeamWonTossAndWonMatch={};

    for (let index=0;index<allMatches.length;index++){
        let tossWinner=allMatches[index].toss_winner;
        let matchWinner=allMatches[index].winner;
        
        if (tossWinner==matchWinner){
            
            if (!TeamWonTossAndWonMatch[tossWinner]){
                TeamWonTossAndWonMatch[tossWinner]=1;
            }
            
            else{
                TeamWonTossAndWonMatch[tossWinner]+=1;
                
            }
        }
    }
    return(TeamWonTossAndWonMatch);
}    

module.exports=teamWinTossAndWinMatch;